import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/model/utente';
import { NgForm } from '@angular/forms';
import { PrimoServizioService } from 'src/app/services/primo-servizio.service';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.css'],
})
export class InputOutputComponent implements OnInit {


  utenteParent: Utente ;
  messaggio: string ="Nessun Submit"
  
  constructor(private servizio:PrimoServizioService) { }

  ngOnInit() {
    this.utenteParent = new Utente("Nunzio","Autiero",27);
    console.log(this.servizio._stringaCondivisa)
  }

  submitPadre(form:NgForm){
    if(form.valid){
      this.messaggio ="Form Valido"
    } else {
      this.messaggio = "Form non Valido"
    }
  }

}
