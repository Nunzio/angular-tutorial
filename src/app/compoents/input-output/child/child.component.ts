import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Utente } from 'src/app/model/utente';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {


  @Input() utenteFiglio:Utente;
  @Output() submitEmitter : EventEmitter<NgForm> = new EventEmitter<NgForm>();

  constructor() { }

  ngOnInit() {
  }

  // onSubmit(form:NgForm){
  //   this.submitEmitter.emit(form)
  // }

}
