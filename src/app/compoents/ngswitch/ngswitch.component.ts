import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/model/utente';

@Component({
  selector: 'app-ngswitch',
  templateUrl: './ngswitch.component.html',
  styleUrls: ['./ngswitch.component.css']
})
export class NgswitchComponent implements OnInit {
  utente : Utente;

  constructor() { }

  ngOnInit() {
    this.utente = new Utente("isdnifnsi","Autiero",27);
  }

}
