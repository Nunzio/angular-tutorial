import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Utente } from 'src/app/model/utente';
import { NgForm } from '@angular/forms';
import { PrimoServizioService } from 'src/app/services/primo-servizio.service';

@Component({
  selector: 'nunzio',
  templateUrl: './primo-component.component.html',
  styleUrls: ['./primo-component.component.css'],
})
export class PrimoComponentComponent implements OnInit, OnDestroy {

  utente: Utente;
  visibile: boolean = false;
  // @ViewChild("myForm") myForm;



  constructor(private servizio:PrimoServizioService) { }

  ngOnInit() {
    this.utente = new Utente("Alessandro", "Alfieri", 33)
    console.log(this.servizio.getArray())
  }

  ngOnDestroy() {
    console.log("lasciato il componente")
  }

  changeVisibile() {
    this.visibile = !this.visibile
    console.log(this.visibile)
  }


  onSubmit(myForm: NgForm) {
    if (myForm.valid)
      console.log(myForm)
    else {
      console.log("Form non valido")
    }
  }


  submitPrimoComponente(){
    this.servizio.pushElement();
    console.log(this.servizio.getArray())
  }




}
