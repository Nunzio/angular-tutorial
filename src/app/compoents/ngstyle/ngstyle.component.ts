import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-ngstyle',
  templateUrl: './ngstyle.component.html',
  styleUrls: ['./ngstyle.component.css']
})
export class NgstyleComponent implements OnInit {

  myWidth:number=100;
  myColor:string=""

  @ViewChild("mioDiv") mioDiv : ElementRef ;
  @ViewChild("input") mioInput : ElementRef

  constructor() { }

  ngOnInit() {
    this.mioDiv.nativeElement.innerHTML ="<input type='text'/>"
  }

}
