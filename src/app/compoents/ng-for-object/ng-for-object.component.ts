import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/model/utente';

@Component({
  selector: 'app-ng-for-object',
  templateUrl: './ng-for-object.component.html',
  styleUrls: ['./ng-for-object.component.css']
})
export class NgForObjectComponent implements OnInit {
  arrayObject : Array<Utente> =[];
  utente : Utente;
  constructor() { }

  ngOnInit() {
    this.utente = new Utente("Nunzio","Autiero",27);
    this.arrayObject.push(this.utente)
    this.arrayObject.push(new Utente("Alessandro","Alfieri",33))
    this.arrayObject.push(new Utente("Federico","Esposito",32))
  }

}
