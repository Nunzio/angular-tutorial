import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngclass',
  templateUrl: './ngclass.component.html',
  styleUrls: ['./ngclass.component.css']
})
export class NgclassComponent implements OnInit {

  constructor() { }

  miaCondizione:boolean=true;
  miaStringa :string ="a";
  ngOnInit() {
  }

  cambiaValore(){
    this.miaCondizione= !this.miaCondizione
  }

}
