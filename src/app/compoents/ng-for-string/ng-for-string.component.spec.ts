import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgForStringComponent } from './ng-for-string.component';

describe('NgForStringComponent', () => {
  let component: NgForStringComponent;
  let fixture: ComponentFixture<NgForStringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgForStringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgForStringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
