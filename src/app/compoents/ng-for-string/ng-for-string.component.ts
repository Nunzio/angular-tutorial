import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for-string',
  templateUrl: './ng-for-string.component.html',
  styleUrls: ['./ng-for-string.component.css']
})
export class NgForStringComponent implements OnInit {
  arrayStringhe:Array<string> = ['primo',"secondo",'terzo'];
  constructor() { }

  ngOnInit() {
  }

  aggiungiElemento(){
    let stringa:string="nuova stringa"
    this.arrayStringhe.push(stringa);
  }

  rimuoviUltimo(){
    this.arrayStringhe.pop()
  }

  rimuovi(index:number){
    this.arrayStringhe.splice(index,1)
  }

  modifica(index:number){
    let stringa:string ="Stringa Mofidicata"
    this.arrayStringhe.splice(index,1,stringa)
  }

}
