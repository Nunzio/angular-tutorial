import { Component, OnInit } from '@angular/core';
import { PrimoServizioService } from 'src/app/services/primo-servizio.service';

@Component({
  selector: 'app-servizi',
  templateUrl: './servizi.component.html',
  styleUrls: ['./servizi.component.css']
})
export class ServiziComponent implements OnInit {

  constructor(private serv : PrimoServizioService) { }

  mioArray :Array<string> = []

  ngOnInit() {
    this.mioArray = this.serv.getArray();
  }

}
