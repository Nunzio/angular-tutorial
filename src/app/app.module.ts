import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { PrimoComponentComponent } from './compoents/primo-component/primo-component.component';
import { NavbarComponent } from './compoents/navbar/navbar.component';
import { AppRouterModule } from './app-router.module';
import { FormComponent } from './compoents/form/form.component';
import { NgifComponent } from './compoents/ngif/ngif.component';
import { NgswitchComponent } from './compoents/ngswitch/ngswitch.component';
import { NgForStringComponent } from './compoents/ng-for-string/ng-for-string.component';
import { NgForObjectComponent } from './compoents/ng-for-object/ng-for-object.component';
import { NgstyleComponent } from './compoents/ngstyle/ngstyle.component';
import { NgclassComponent } from './compoents/ngclass/ngclass.component';
import { InputOutputComponent } from './compoents/input-output/input-output.component';
import { ChildComponent } from './compoents/input-output/child/child.component';
import { ServiziComponent } from './compoents/servizi/servizi.component';
import { PrimoServizioService } from './services/primo-servizio.service';


@NgModule({
  declarations: [
    AppComponent,
    PrimoComponentComponent,
    NavbarComponent,
    FormComponent,
    NgifComponent,
    NgswitchComponent,
    NgForStringComponent,
    NgForObjectComponent,
    NgstyleComponent,
    NgclassComponent,
    InputOutputComponent,
    ChildComponent,
    ServiziComponent,
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    FormsModule
  ],
  providers: [PrimoServizioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
