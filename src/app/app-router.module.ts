import {Routes, RouterModule} from '@angular/router'
import { PrimoComponentComponent } from './compoents/primo-component/primo-component.component';
import { NgModule } from '@angular/core';
import { FormComponent } from './compoents/form/form.component';
import { NgifComponent } from './compoents/ngif/ngif.component';
import { NgswitchComponent } from './compoents/ngswitch/ngswitch.component';
import { NgForStringComponent } from './compoents/ng-for-string/ng-for-string.component';
import { NgForObjectComponent } from './compoents/ng-for-object/ng-for-object.component';
import { NgstyleComponent } from './compoents/ngstyle/ngstyle.component';
import { NgclassComponent } from './compoents/ngclass/ngclass.component';
import { InputOutputComponent } from './compoents/input-output/input-output.component';
import { ServiziComponent } from './compoents/servizi/servizi.component';

const route : Routes =[
    {path:"", redirectTo:"/home",pathMatch:"full"},
    {path:"home", component:PrimoComponentComponent},
    {path:"form", component:FormComponent},
    {path:"ngif", component:NgifComponent },
    {path:"ngswitch", component:NgswitchComponent},
    {path:"ngstyle", component:NgstyleComponent},
    {path:"ngclass", component:NgclassComponent},
    {path:"array/string", component:NgForStringComponent},
    {path:"array/object", component:NgForObjectComponent},
    {path:"input-output", component:InputOutputComponent},
    {path:"servizi", component:ServiziComponent},


]

@NgModule({
    imports:[RouterModule.forRoot(route)],
    exports:[RouterModule]
})
export class AppRouterModule{
}