import { Injectable } from '@angular/core';

@Injectable()
export class PrimoServizioService {


   _stringaCondivisa :string ="STRINGA CONDIVISA"
   private _arrayString :Array<string> = ['Nunzio','Alessandro','Federico']


  getStringa() : string {
    return this._stringaCondivisa
  }

  getArray() :Array<string>{
    return this._arrayString
  }

  pushElement(){
    let stringa:string="Nuovo Elemento"
    this._arrayString.push(stringa)
  }


  constructor() { }

}
