import { TestBed } from '@angular/core/testing';

import { PrimoServizioService } from './primo-servizio.service';

describe('PrimoServizioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimoServizioService = TestBed.get(PrimoServizioService);
    expect(service).toBeTruthy();
  });
});
